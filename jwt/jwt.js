'use strict'

const JWT = require('jsonwebtoken')

const SECRET_STRING = 'my_super_secret_string' // Must be in a config file or enviroment

// Create Token
const sign = (payload, next) => {
	JWT.sign(payload, SECRET_STRING, {
		expiresIn: '100d'
	}, (err, token) => {
		if (err) {
		    console.error(err)
		}
		next(token)
	})
}

// Token verification
const verify = (token, next) => {
	JWT.verify(token, SECRET_STRING, (err, payload) => {
		if (err) {
		    console.error(err) // This error should be showed only in debug
		}
		next(payload)
	})
}

module.exports = {
	sign,
	verify
}
