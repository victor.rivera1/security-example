'use strict'

const Express = require('express')
const jwt = require('./jwt.js')

const app = Express()

// Add this middleware to all routes that we need to protect
const verifyToken = (req, res, next) => {
	const token = req.headers.authorization.replace('Bearer ', '')
	jwt.verify(token, (payload) => {
		if (!payload) {
			res.status(401).json({ error: 'Session has expired.' })
			return
		}

		req.session = {
			userId: payload.uid
		}
		next()
	})
}

// Apply this to protect all routes except /auth
const verifyTokenToAll = (req, res, next) => {
	const unprotected = ['/login'] // This should be in config file or database
	if (unprotected.includes(req.path)) {
		next()
		return
	}

	const token = req.headers.authorization.replace('Bearer ', '')
	jwt.verify(token, (payload) => {
		if (!payload) {
			res.status(401).json({ error: 'Session has expired.' })
			return
		}

		req.session = {
			userId: payload.uid
		}
		next()
	})
}
app.use(verifyTokenToAll)

// API login
app.post('/login', (req, res) => {
	// Logic login
	let userId = 'thisissomeid' // Id user from logic login

	// Data to sign
	let payload = {
		uid: userId
	}
	jwt.sign(payload, token => res.json({ token }))
})

// API using jwt validation
app.get('/users', verifyToken, (req, res) => {
	res.json({ userId: req.session.userId })
})

app.listen(3000, () => console.log('Server started'))
