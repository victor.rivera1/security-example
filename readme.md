Changes required to continue with the project.

It includes:
- JWT (Json Web Token), for validation of users are logged
- bcrypt, encryption of passwords (it is not possible to decipher)
- crypto, encryption and decryption of sensitive information