'use strict'

const Crypto = require('crypto')

const algorithm = 'aes-256-ctr'
const ENCRYPTION_KEY = 'my_super_secret_string1234567890' // Must be in a config file or enviroment (must be 32 characters)

const encrypt = (data) => {
	let iv = Crypto.randomBytes(16)
	let cipher = Crypto.createCipheriv(algorithm, Buffer.from(ENCRYPTION_KEY), iv)
	let encrypted = cipher.update(data.toString())

	encrypted = Buffer.concat([encrypted, cipher.final()])
	return iv.toString('hex') + ':' + encrypted.toString('hex')
}

const decrypt = (data) => {
	let textParts = data.toString().split(':')
	let iv = Buffer.from(textParts.shift(), 'hex')
	let encryptedText = Buffer.from(textParts.join(':'), 'hex')
	let decipher = Crypto.createDecipheriv(algorithm, Buffer.from(ENCRYPTION_KEY), iv)
	let decrypted = decipher.update(encryptedText)

	decrypted = Buffer.concat([decrypted, decipher.final()])
	return decrypted.toString()
}

module.exports = {
	encrypt,
	decrypt
}
