'use strict'

const Express = require('express')
const BodyParser = require('body-parser')
const crypto = require('./crypto.js')

const app = Express()

app.use(BodyParser.json())

app.post('/add-card', (req, res) => {
	let month = req.body.month
	// ....

	month = crypto.encrypt(month) // Always return a string
	// Save month in db
	// ....

	res.json({ month })
})

app.get('/cards', (req, res) => {
	// Retrieve cards
	// ...
	let cards = [{
		month: '98e8f6a5eb2dfae061ad17fba5403301:043c' // Example month saved in db
	}]

	cards = cards.map(card => {
		card.month = parseInt(crypto.decrypt(card.month)) // decrypt and convert to int
		// Any other field encrypted
		// ...

		return card
	})

	res.json(cards)
})

app.listen(3000, () => console.log('Server started'))
