'use strict'

const Express = require('express')
const BodyParser = require('body-parser')
const Bcrypt = require('bcrypt')

const SALT_RAUNDS = 10

const app = Express()

app.use(BodyParser.json())

app.post('/register', (req, res) => {
	let password = req.body.password
	Bcrypt.hash(password, SALT_RAUNDS, (err, hash) => {
		res.json({ password: hash }) // The hash is going to be saved at the db with the user
	})
})

app.post('/login', (req, res) => {
	let email = req.body.email
	let password = req.body.password

	// We search for the user by email and we obtain the password hash stored in the database
	// Ex: contraseña -> $2b$10$itVc71HJKM/qlXZY8plDSuGi5U7TiiWZmOG1Ng1jExTVmRu9VU4VK
	let passwordInDB = '$2b$10$itVc71HJKM/qlXZY8plDSuGi5U7TiiWZmOG1Ng1jExTVmRu9VU4VK'

	Bcrypt.compare(password, passwordInDB, (err, isValid) => {
		if (isValid) {
			// Get jwt
			res.json({ token: 'some token' })
		} else {
			res.json({ error: 'Invalid email or password' })
		}
	})
})

app.listen(3000, () => console.log('Server started'))
